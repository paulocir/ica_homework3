function [A1, A2, A3, A4, A5, A6, A7, A8, A9, A10] = Amostras(M)

testing = M(:,1:end-1);
testing =testing';
Class1 = [];
k = 1;
for n=1:50
    aux = testing(:,k:k+1);
    Class1 = [Class1; aux];
    k=k+2;
end
i =1;
outcome =[];
controle = 1;
for n=1:40000
    if controle ==0
        controle =1;
        i=i+1;
        if i==6
            i=1;
        end
        
    end
    if  mod(n,800)==0
        controle = 0;
        
    end
    
    outcome = [outcome;i];
end

DataMatrix = [Class1 outcome];
    A1= DataMatrix(1:4000,:);
    A2 = DataMatrix(4001:8000,:);
    A3 = DataMatrix(8001:12000,:);
    A4 = DataMatrix(12001:16000,:);
    A5 = DataMatrix(16001:20000,:);
    A6 = DataMatrix(20001:24000,:);
    A7 = DataMatrix(24001:28000,:);
    A8 = DataMatrix(28001:32000,:);
    A9 = DataMatrix(32001:36000,:);
    A10 = DataMatrix(36001:40000,:);

end
