function result = SoftMax( x )

result = bsxfun(@rdivide,exp(x),sum(exp(x),2));
 
end

