function StemPlot(A1,Xlabel,Ylabel,nameFile)

    stem(A1(3201:4000,1:1),'Color',[0 0 1]);
    hold on
    stem(A1(3201:4000,2:2),'Color',[1 0 0]);
    

    legend('Sensor 1','Sensor 2')
    hold off


    h=gcf;
    xlabel(Xlabel,'FontSize',12,'FontWeight','bold') ;
    ylabel(Ylabel,'FontSize',12,'FontWeight','bold') ;
    
    set(h,'PaperOrientation','landscape');
    set(h,'PaperUnits','normalized');
    set(h,'PaperPosition', [0 0 1 1]);
    print(gcf, '-dpdf', nameFile);
    close;
    
   
    
end
