clear; close all ; clc

load PauloDS.csv
load MauriDS.csv
load Raul.csv
load Clara.csv
load Beroni.csv
load Jene.csv
load Leticia.csv


[xTrain, yTrain, xTest, yTest] = OitentaVinte(Leticia);
y1 = categorical(yTrain);
B = mnrfit(xTrain,y1);

[B1,B2,B3,B4,B5] = OneAgainstAllFiveClass(xTrain,yTrain);

p1 = mnrval(B1, xTest);
p2 = mnrval(B2, xTest);
p3 = mnrval(B3, xTest);
p4 = mnrval(B4, xTest);
p5 = mnrval(B5, xTest);

p1=p1(:,1);


p2=p2(:,1);
p3=p3(:,1);
p4=p4(:,1);
p5=p5(:,1);
confmat = Confusion(yTest,[p1 p2 p3 p4 p5]);
plotConfMat(confmat, {'Gesto 1', 'Gesto 2', 'Gesto 3', 'Gesto 4', 'Gesto 5'});
erro= 0;
yModelo = [];
for i=1:size(yTest)
    v = [p1(i) p2(i) p3(i) p4(i) p5(i)];
    pos = find(v==max(v));
    yModelo = [yModelo; pos];
    if pos~=yTest(i)
        erro = erro+1;
    end

    
end

Acerto = (size(yTest)-erro)/size(yTest)*100;


%COMPARANDO COM FUNCAO MATLABISTICA:

p1 = mnrval(B,xTest);


erro = 0;


for j=1:size(yTest)
    who  = find(p1(j,:)==max(p1(j,:)));
    q= yTest(j);
    if who~=q
        erro= erro+1;
    end
 
end

AcertoMatlab = (size(yTest)-erro)/size(yTest)*100;





