function ConfMatrix = Confusion(yTest,yPredicted)
    con = zeros(5,5);
    controle = 0;
    classe= 1;
    for i=1:size(yTest,1)
         if(mod(i,800)==0)
             controle=1;
         end

                 
         aux = find(yPredicted(i,:)==max(yPredicted(i,:)));
         con(classe,aux) = con(classe,aux)+1;        
         
         if controle==1
            classe = classe+1;
            controle =0;
            if classe>5
                classe= 1;
            end
         end
    end
    ConfMatrix = con;

end

