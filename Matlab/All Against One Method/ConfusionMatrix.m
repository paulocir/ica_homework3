function ConfusionMatrix()

targetsVector = [1 2 1 1 3 2 3]; % True classes
outputsVector = [1 3 1 2 3 1 3]; % Predicted classes
% Convert this data to a [numClasses x 6] matrix
targets = zeros(4,7);
outputs = zeros(4,7);
targetsIdx = sub2ind(size(targets), targetsVector, 1:7);
outputsIdx = sub2ind(size(outputs), outputsVector, 1:7);
targets(targetsIdx) = 1;
outputs(outputsIdx) = 1;
% Plot the confusion matrix for a 3-class problem
plotconfusion(targets,outputs)


end

