function [ xTrain, yTrain, xTest, yTest ] = OitentaVinte( x )

[A1, A2, A3, A4, A5, A6, A7, A8, A9, A10] = Amostras(x);

xTrain = [A1(:,1:2);A2(:,1:2);A3(:,1:2);A4(:,1:2);A5(:,1:2);A6(:,1:2);A7(:,1:2);A8(:,1:2)];
xTest = [A9(:,1:2);A10(:,1:2)];

yTrain = [A1(:,3);A2(:,3);A3(:,3);A4(:,3);A5(:,3);A6(:,3);A7(:,3);A8(:,3)];
yTest = [A9(:,3);A10(:,3)];

end

