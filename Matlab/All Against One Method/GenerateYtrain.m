function output = GenerateYtrain(y)
            v = [1 -1 -1 -1 -1;-1 1 -1 -1 -1;-1 -1 1 -1 -1;-1 -1 -1 1 -1;-1 -1 -1 -1 1];
            bigMatrix =[];
            aux = 1;
            for i=1:size(y)
                bigMatrix = [bigMatrix; v(aux,1:end)];                
                if i~=1
                    if mod(i,800)==0
                        aux = aux+1;
                        if aux == 6
                            aux = 1;
                        end
                    end
                end
                
                
            end
            output = bigMatrix;
end