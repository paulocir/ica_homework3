function [B1,B2,B3,B4,B5] = OneAgainstAllFiveClass(x,y)

 
 yAux = y;
 for i=1:size(y)
     if yAux(i)~=1
         yAux(i) = 0;
     end
 end
  
 B1 = MyLogisticRegression(x,yAux); 
 
  yAux = y;
 for i=1:size(y)
     if yAux(i)~=2
         yAux(i) = 0;
     else
         yAux(i)=1;
     end
 end
 B2 = MyLogisticRegression(x,yAux); 
 
 yAux = y;
 for i=1:size(y)
     if yAux(i)~=3
         yAux(i) = 0;
     else
         yAux(i)=1;
     end
 end
 B3 = MyLogisticRegression(x,yAux); 
 
 yAux = y;
 for i=1:size(y)
     if yAux(i)~=4
         yAux(i) = 0;
     else
         yAux(i)=1;
     end
 end
 B4 = MyLogisticRegression(x,yAux); 
 
  yAux = y;
 for i=1:size(y)
     if yAux(i)~=5
         yAux(i) = 0;
     else
         yAux(i)=1;
     end
 end
 B5 = MyLogisticRegression(x,yAux); 

end

