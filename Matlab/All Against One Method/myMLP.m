classdef myMLP <handle
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access = private)           

            %PAULO:
            learningRate;
            historicoMSE = [];
            precisao;
            hiddenLayer;
            nNeuronSaida;
            maxEpoch;  
            mediaA=0;
            Epoch;
            AtualWeights1;
            AtualWeights2;
            AtualWeights3;
            AtualWeights4;

    end
    
    methods
        
        
        function instance = myMLP(tax,prec,hid,nns,mEp)
            instance.learningRate = tax;
            instance.precisao = prec;
            instance.hiddenLayer = hid;
            instance.nNeuronSaida = nns;
            instance.maxEpoch = mEp;
            instance.AtualWeights1 = rand(instance.hiddenLayer(1),3) -0.5;
            instance.AtualWeights2 = rand(instance.hiddenLayer(2),instance.hiddenLayer(1)+1) -0.5;
            instance.AtualWeights3 = rand(instance.hiddenLayer(3),instance.hiddenLayer(2)+1) -0.5;
            instance.AtualWeights4 = rand(instance.nNeuronSaida,instance.hiddenLayer(3)+1) -0.5;
            
        
        end 
        
        function r = getHistorico(obj)
            r=obj.historicoMSE;
        end
        
        function plotarhistoricoMSE(obj,frase,name)
            figure();
            plot(1:obj.Epoch,obj.historicoMSE);
            title(frase);
            grid;
            h=gcf;
            xlabel('Epoch','FontSize',12,'FontWeight','bold') ;
            ylabel('MSE','FontSize',12,'FontWeight','bold') ;
            
            set(h,'PaperOrientation','landscape');
            set(h,'PaperUnits','normalized');
            set(h,'PaperPosition', [0 0 1 1]);
            print(gcf, '-dpdf', name);
            close;
        end

        
        function r = getEpoch(obj)
            r =obj.Epoch;
        end
        
        function output = GenerateYtrain(obj,y)
            v = [1 -1 -1 -1 -1;-1 1 -1 -1 -1;-1 -1 1 -1 -1;-1 -1 -1 1 -1;-1 -1 -1 -1 1];
            bigMatrix =[];
            aux = 1;
            for i=1:size(y)
                bigMatrix = [bigMatrix; v(aux,1:end)];                
                if i~=1
                    if mod(i,800)==0
                        aux = aux+1;
                        if aux == 6
                            aux = 1;
                        end
                    end
                end
                
                
            end
            output = bigMatrix;
        end
        
        function r = MinMax(obj,x)
            minimo = min(x);
            maximo = max(x);
            for i=1:size(x)
                if x(i)>4000
                    x(i) = 4000;
                end
            end
            
            r = (x-minimo)./(maximo-minimo);
            r = (x-minimo)./(maximo-minimo);
        end
        
        
        function output = BiasGenerator(obj,m)
            bias = ones(size(m,1),1)*-1;            
            output = [bias m];            
        end
        
        function d = TanhDerivative(obj,x)
            aux = tanh(x).^2;
            d = 1-aux;
        end
        
        function yPredicted = FeedFoward(obj,xTest,Normalized)
            QtdAmostras = size(xTest,1);
            yp = [];
            if Normalized == 1
                for i=1:size(xTest,2)
                    xTest(:,i) = obj.MinMax(xTest(:,i));
                end                
            end
            
            BiasedXtest = obj.BiasGenerator(xTest);
            ExpectedArray=[1 -1 -1 -1 -1;-1 1 -1 -1 -1;-1 -1 1 -1 -1;-1 -1 -1 1 -1;-1 -1 -1 -1 1];
            
            for i=1:QtdAmostras
                    I1 = obj.AtualWeights1*(BiasedXtest(i,:))';
                    Y1 = -1;
                    Y1 = [Y1;tanh(I1)];
                    
                    I2 = obj.AtualWeights2 * Y1;
                    Y2 = -1;
                    Y2 = [Y2;tanh(I2)];
                    
                    I3 = obj.AtualWeights3 * Y2;
                    Y3 = -1;
                    Y3 = [Y3;tanh(I3)];
                    
                    I4 = obj.AtualWeights4 * Y3;                    
                    Y4 = tanh(I4);
                    yp = [yp; Y4'];
                    
                    
                
                
            end
            
            yPredicted = yp;
        end
        
        function Train(obj,xTrain,yTrain,Normalized)                        
            yTrainMatrix = obj.GenerateYtrain(yTrain);
            QtdAmostras = size(xTrain,1);
            obj.Epoch = 0;
            ep =0;
            MSE1 = 0;
            MSE2 = obj.precisao +1;
            
            if Normalized ==1
                for i=1:size(xTrain,2)
                    xTrain(:,i) = obj.MinMax(xTrain(:,i));
                end                
            end
            BiasedXtrain = obj.BiasGenerator(xTrain);
            mediaA = 0;
            
            Acc1 = zeros(obj.hiddenLayer(1),3);
            Acc2 = zeros(obj.hiddenLayer(2),obj.hiddenLayer(1)+1);
            Acc3 = zeros(obj.hiddenLayer(3),obj.hiddenLayer(2)+1);
            Acc4 = zeros(obj.nNeuronSaida,obj.hiddenLayer(3)+1);

            while((obj.Epoch<obj.maxEpoch)&&(abs(MSE2-MSE1)>obj.precisao))
                Acc1 = zeros(obj.hiddenLayer(1),3);
                Acc2 = zeros(obj.hiddenLayer(2),obj.hiddenLayer(1)+1);
                Acc3 = zeros(obj.hiddenLayer(3),obj.hiddenLayer(2)+1);
                Acc4 = zeros(obj.nNeuronSaida,obj.hiddenLayer(3)+1);
                
                MSE1 = MSE2;
                
                for i=1:QtdAmostras
                    I1 = obj.AtualWeights1*(BiasedXtrain(i,:))';
                    Y1 = -1;
                    Y1 = [Y1;tanh(I1)];
                    
                    I2 = obj.AtualWeights2 * Y1;
                    Y2 = -1;
                    Y2 = [Y2;tanh(I2)];
                    
                    I3 = obj.AtualWeights3 * Y2;
                    Y3 = -1;
                    Y3 = [Y3;tanh(I3)];
                    
                    I4 = obj.AtualWeights4 * Y3;                    
                    Y4 = tanh(I4);
                    
                    line = yTrainMatrix(i,1:end)';
                    
                    Grad4 = (line-Y4).*(obj.TanhDerivative(I4));                    
                    Acc4 = Acc4 + ((obj.learningRate*Grad4) * Y3');
                                        
                    aux = (obj.AtualWeights4(:,2:end))'*Grad4;
                    
                    Grad3 = aux.*(obj.TanhDerivative(I3));
                    Acc3 = Acc3 + ((obj.learningRate*Grad3) * Y2');
                    
                    aux = (obj.AtualWeights3(:,2:end))'*Grad3;
                    
                    Grad2 = aux.*(obj.TanhDerivative(I2));
                    Acc2 = Acc2 + ((obj.learningRate*Grad2) * Y1');
                    
                    aux = (obj.AtualWeights2(:,2:end))'*Grad2;                    
                    
                    Grad1 = aux.*(obj.TanhDerivative(I1));
                    Acc1 = Acc1 + ((obj.learningRate*Grad1)*(BiasedXtrain(i,:)));
                    
                end

                
                obj.AtualWeights1 = obj.AtualWeights1 + (Acc1/QtdAmostras);
                obj.AtualWeights2 = obj.AtualWeights2 + (Acc2/QtdAmostras);
                obj.AtualWeights3 = obj.AtualWeights3 + (Acc3/QtdAmostras);
                obj.AtualWeights4 = obj.AtualWeights4 + (Acc4/QtdAmostras);
                
                err =0;
                
                for i=1:QtdAmostras
                    I1 = obj.AtualWeights1*(BiasedXtrain(i,:))';
                    Y1 = -1;
                    Y1 = [Y1;tanh(I1)];
                    
                    I2 = obj.AtualWeights2*Y1;
                    Y2 = -1;
                    Y2 = [Y2;tanh(I2)];
                    
                    I3 = obj.AtualWeights3*Y2;
                    Y3 = -1;
                    Y3 = [Y3;tanh(I3)];
                    
                    I4 = obj.AtualWeights4*Y3;
                    Y4 = tanh(I4);
                    
                    line = yTrainMatrix(i,1:end)';
                    
                    err = err + (sum((line-Y4).^2))/2;
                    
                end
                
                MSE2 = err/QtdAmostras;
                
                obj.historicoMSE = [obj.historicoMSE MSE2];
                obj.Epoch = obj.Epoch+1;
                ep=ep+1
                
            end          
        end
        
    end
    
end

