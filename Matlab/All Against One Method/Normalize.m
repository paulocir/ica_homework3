function [ xTrain,xTest ] = Normalize( x1,x2 )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    maximo1 = max(max(x1));
    maximo2 = max(max(x2));
    minimo1 = min(min(x1));
    minimo2 = min(min(x2));
    if minimo1<=minimo2
        minimo = minimo1
    else
        minimo = minimo2
    end
    if maximo1 >= maximo2
        maximo = maximo1;
    else
        maximo = maximo2;
    end
    xTrain = (x1-minimo)./(maximo-minimo);
    
    xTest = (x2-minimo)./(maximo-minimo);

end

