clear; close all ; clc

load PauloDS.csv
load MauriDS.csv
load Raul.csv
load Clara.csv
load Beroni.csv
load Jene.csv
load Leticia.csv
load Sharon.csv

[xTrain, yTrain, xTest, yTest] = OitentaVinte(Clara);
yTest = GenerateYtrain(yTest);
[xTrain, xTest] = Normalize(xTrain,xTest);
%best config: 0.02,0.0001,[4,6,5],5,5000
m = myMLP(0.13,0.00001,[10,10,10],5,10000);

tic
m.Train(xTrain,yTrain,0); 
toc
epocas1 = m.getEpoch();
m.plotarhistoricoMSE('Curva de Erro','Clara0000');
err1 = m.getHistorico();
yPredicted = m.FeedFoward(xTest,0);
%yPredicted = AdjustedPredicted(yPredicted);
confmat = Confusion(yTest,yPredicted);
plotConfMat(confmat, {'Gesto 1', 'Gesto 2', 'Gesto 3', 'Gesto 4', 'Gesto 5'});
erro = 0;
for i=1:size(yTest,1)
    maxiP = find(yPredicted(i,:)==max(yPredicted(i,:)));
    maxiT = find(yTest(i,:)==max(yTest(i,:)));
    if maxiP~=maxiT
        erro = erro+1;
    end
    
end

AcertoMLP = (size(yTest,1)-erro)/size(yTest,1) * 100;

    

