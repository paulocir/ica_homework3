function BigScatterPlot(A1,Xlabel,Ylabel,ZClass,nameFile) 


    
    sz = 50;
    n2 = 8000;
    n1 = 1;
    ar1 = A1(n1:n2,1:1);
    ar2=A1(n1:n2,2:2);
    ar3=A1(n1:n2,3:3);
    
%     [x, y] = meshgrid(0:5000:5000);
%     z = 3*ones(size(x,1));
% 
%     surf(x,y,z,'FaceAlpha',0.09);
%     hold on
%     
%     [x, y] = meshgrid(0:5000:5000);
%     z = 4*ones(size(x,1));
% 
%     surf(x,y,z,'FaceAlpha',0.09);
%     hold on
%     
%    
%    
%     


    
    scatter3(ar1,ar2,ar3,sz,'MarkerEdgeColor',[.5 0 .3],'MarkerFaceColor',[.7 0 .5],'LineWidth',0.5)
    hold on   
    
    n1 = n2+1;
    n2 = 8000+n2;
    ar1 = A1(n1:n2,1:1);
    ar2=A1(n1:n2,2:2);
    ar3=A1(n1:n2,3:3);
   
    
    scatter3(A1(n1:n2,1:1),A1(n1:n2,2:2),A1(n1:n2,3:3),sz,'MarkerEdgeColor',[0 .3 .5],'MarkerFaceColor',[0 .5 .7],'LineWidth',0.5)
    hold on
    
    n1 = n2+1;
    n2 = 8000+n2;
    ar1 = A1(n1:n2,1:1);
    ar2=A1(n1:n2,2:2);
    ar3=A1(n1:n2,3:3);
    scatter3(A1(n1:n2,1:1),A1(n1:n2,2:2),A1(n1:n2,3:3),sz,'MarkerEdgeColor',[.5 .3 0],'MarkerFaceColor',[.7 .5 0],'LineWidth',0.5)
    hold on
    n1 = n2+1;
    n2 = 8000+n2;
    ar1 = A1(n1:n2,1:1);
    ar2=A1(n1:n2,2:2);
    ar3=A1(n1:n2,3:3);
    scatter3(A1(n1:n2,1:1),A1(n1:n2,2:2),A1(n1:n2,3:3),sz,'MarkerEdgeColor',[.3 .5 0],'MarkerFaceColor',[.5 .7 0],'LineWidth',.5)
    hold on
    
    n1 = n2+1;
    n2 = 8000+n2;
    ar1 = A1(n1:n2,1:1);
    ar2=A1(n1:n2,2:2);
    ar3=A1(n1:n2,3:3);
    scatter3(A1(n1:n2,1:1),A1(n1:n2,2:2),A1(n1:n2,3:3),sz,'MarkerEdgeColor',[0 .5 .3],'MarkerFaceColor',[0 .7 .5],'LineWidth',0.5)
   % legend('Classe 1','Classe 2','Classe 3','Classe 4', 'Clase 5')
    xlim([0 4500]);
    ylim([0 4500]);
    zlim([0 6]);
    legend('Classe 1','Classe 2','Classe 3','Classe 4', 'Classe 5')
    hold off


    h=gcf;
    xlabel(Xlabel,'FontSize',12,'FontWeight','bold') ;
    ylabel(Ylabel,'FontSize',12,'FontWeight','bold') ;
    zlabel(ZClass,'FontSize',12,'FontWeight','bold') ;
    set(h,'PaperOrientation','landscape');
    set(h,'PaperUnits','normalized');
    set(h,'PaperPosition', [0 0 1 1]);
    print(gcf, '-dpdf', nameFile);
    close;
    
   
    
end
