function [ nBeta,num_iter ] = gradDesc( x,y,beta )

    lr = 0.1;
    converge_change=0.001;
    cost = cost_func(beta,x,y);
    change_cost=1;
    num_iter=1;
    while change_cost>converge_change
        old_cost = cost;
        aux=log_gradient(beta,x,y);
        beta = beta - (lr);
        
    end
    
     nBeta =1;
     num_iter = 21;
end

