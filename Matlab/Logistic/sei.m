clear; close all ; clc


%JUST TESTING:
load data.csv

y=randi(2,[10,1])-1;
 % initialize



x=data(:, [1, 2]);

ytrain=data(:,3); % Target variable
y=data(:,3); % Target variable
alpha = 0.1;
theta = zeros(1,size(x,2));
J = NaN(size(x,1),1);




htheta = sigmoid(x*theta');

for n=1:100
    aux  = htheta'-y;
    grad = x' * (aux); % gradient
    theta = theta - alpha*grad'; % update theta
    htheta = sigmoid(x*theta');
    J(n) = sum(-y'*log(htheta)) - sum((1-y)' * log(1 - htheta)); % cost function
end
 
% xtrain=zscore(x(:,1:end-1));% Normalized Predictors

xtrain = [ones(size(x,1),1) x];
beta = zeros(size(xtrain,2),1);

%Cost function:
gradDesc(xtrain,ytrain,beta);

h = sigmoid(xtrain*beta);




y =categorical(ytrain);
B = mnrfit(x,y)

