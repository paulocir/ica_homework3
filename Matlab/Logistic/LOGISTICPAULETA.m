close all;
clc;
clear;

load data.csv

x=data(:, [1, 2]);
y=data(:,3); % Target variable

y1 =categorical(y);
B = mnrfit(x,y1);

g = inline('1.0 ./ (1.0 + exp(-z))');

iterations = 100;
learningrate = 0.1;
err = 0;
acc = 0;
J = zeros(iterations,1);

[m,n] = size(x);


temp_data = [ones(m,1), x];

label_one = find(y);
label_zero = find(y==0);
theta = zeros(n+1,1);



for i = 1:iterations
    % Hypothesis function h(x)
    h = g(temp_data * theta);
    
    % Gradient
    grdnt = (1/m) .* temp_data' * (h-y);
    
    hessian = (1/m) .* temp_data' * diag(h) * diag(1-h) * temp_data;
    % Update J to prove convergence 
    J(i) = (1/m) * sum(-y.*log(h) - (1-y) .* log(1-h));
    % Update theta, calculate partial derivative of gradient
                
    % using Newton's method :
    gradForGD = 0.0;
    % using Gradient Descent :
    for j = 1:m
        gradForGD = gradForGD + ( h(j) - y(j) ) * temp_data(j, :)';
    end
    % gradient = nx1 column vector
    gradForGD = (1/m) * gradForGD;
    theta = theta - learningrate * gradForGD;  
    
 
    
end





  
