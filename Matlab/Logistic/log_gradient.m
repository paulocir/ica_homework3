function final_calc  = log_gradient( beta, X, y )
    first_calc = sigmoid(X*beta)'-y;
    final_calc = first_calc'.*X;
end

