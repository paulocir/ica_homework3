function MyScatterPlot(A1,Xlabel,Ylabel,ZClass,nameFile) 


    
    sz = 50;

    scatter3(A1(1:800,3:3),A1(1:800,2:2),A1(1:800,1:1),sz,'MarkerEdgeColor',[.5 0 .3],'MarkerFaceColor',[.7 0 .5],'LineWidth',1.5)
    hold on
    scatter3(A1(801:1600,3:3),A1(801:1600,2:2),A1(801:1600,1:1),sz,'MarkerEdgeColor',[0 .3 .5],'MarkerFaceColor',[0 .5 .7],'LineWidth',1.5)
    hold on
    scatter3(A1(1601:2400,3:3),A1(1601:2400,2:2),A1(1601:2400,1:1),sz,'MarkerEdgeColor',[.5 .3 0],'MarkerFaceColor',[.7 .5 0],'LineWidth',1.5)
    hold on
    scatter3(A1(2401:3200,3:3),A1(2401:3200,2:2),A1(2401:3200,1:1),sz,'MarkerEdgeColor',[.3 .5 0],'MarkerFaceColor',[.5 .7 0],'LineWidth',1.5)
    hold on
    scatter3(A1(3201:4000,3:3),A1(3201:4000,2:2),A1(3201:4000,1:1),sz,'MarkerEdgeColor',[0 .5 .3],'MarkerFaceColor',[0 .7 .5],'LineWidth',1.5)
    hold off


    h=gcf;
    xlabel(Xlabel,'FontSize',12,'FontWeight','bold') ;
    ylabel(Ylabel,'FontSize',12,'FontWeight','bold') ;
    zlabel(ZClass,'FontSize',12,'FontWeight','bold') ;
    set(h,'PaperOrientation','landscape');
    set(h,'PaperUnits','normalized');
    set(h,'PaperPosition', [0 0 1 1]);
    print(gcf, '-dpdf', nameFile);
    close;
    
   
    
end
