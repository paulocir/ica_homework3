function Big2DScatterPlot(A1,Xlabel,Ylabel,nameFile) 


    
    sz = 20;
    n2 = 8000;
    n1 = 1;
    scatter(A1(n1:n2,1:1),A1(n1:n2,2:2),sz,'MarkerEdgeColor',[.5 0 .3],'MarkerFaceColor',[.7 0 .5],'LineWidth',1.0)
    hold on
    n1 = n2+1;
    n2 = 8000+n2;
    
    scatter(A1(n1:n2,1:1),A1(n1:n2,2:2),sz,'MarkerEdgeColor',[0 .3 .5],'MarkerFaceColor',[0 .5 .7],'LineWidth',1.0)
    
    n1 = n2+1;
    n2 = 8000+n2;
    scatter(A1(n1:n2,1:1),A1(n1:n2,2:2),sz,'MarkerEdgeColor',[.5 .3 0],'MarkerFaceColor',[.7 .5 0],'LineWidth',1.0)
    
    n1 = n2+1;
    n2 = 8000+n2;
    scatter(A1(n1:n2,1:1),A1(n1:n2,2:2),sz,'MarkerEdgeColor',[.3 .5 0],'MarkerFaceColor',[.5 .7 0],'LineWidth',1.0)
    
    n1 = n2+1;
    n2 = 8000+n2;
    scatter(A1(n1:n2,1:1),A1(n1:n2,2:2),sz,'MarkerEdgeColor',[0 .5 .3],'MarkerFaceColor',[0 .7 .5],'LineWidth',1.0)
    lg = legend('Classe 1','Classe 2','Classe 3','Classe 4', 'Classe 5');
    lg.Location = 'northeastoutside';
    hold off


    h=gcf;
    xlabel(Xlabel,'FontSize',12,'FontWeight','bold') ;
    ylabel(Ylabel,'FontSize',12,'FontWeight','bold') ;    
    set(h,'PaperOrientation','landscape');
    set(h,'PaperUnits','normalized');
    set(h,'PaperPosition', [0 0 1 1]);
    print(gcf, '-dpdf', nameFile);
    close;
    
   
    
end
